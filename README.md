# Theme Picker

Looks in the /themes/custom directory for themes to add to the allowed values list.

## Usage

Add the field field_theme_picker_key to a content type to use the Theme Picker.
