<?php

namespace Drupal\theme_picker\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ThemeHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThemePickerService.
 *
 */
class ThemePickerService implements ContainerInjectionInterface {

  /**
   * Core ThemeHandler Service.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  private $themeHandler;

  /**
   * ThemePickerService constructor.
   *
   * @param \Drupal\Core\Extension\ThemeHandler $themeHandler
   *   Core ThemeHandler.
   */
  public function __construct(ThemeHandler $themeHandler) {
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme_handler')
    );
  }

  /**
   * Installed themes in the custom directory.
   *
   * @return array
   *   Return key|value list of available themes.
   */
  public function getThemes() {
    $custom_themes = [];

    $installed_themes = $this->themeHandler->listInfo();
    $themes = $this->getCustomThemes($installed_themes);

    /** @var \Drupal\Core\Extension\Extension $theme */
    foreach ($themes as $theme) {
      $custom_themes[$theme->getName()] = $theme->info['name'];
    }

    return $custom_themes;
  }

  /**
   * Only the custom installed themes.
   *
   * @param array $themes
   *   Array of enabled themes.
   *
   * @return array
   *   List of enabled custom themes.
   */
  private function getCustomThemes(array $themes) {
    $custom_themes = [];

    foreach ($themes as $theme) {
      if ($this->isCustomTheme($theme)) {
        $custom_themes[] = $theme;
      }
    }

    return $custom_themes;
  }

  /**
   * TRUE if the theme is a custom theme.
   *
   * @param \Drupal\Core\Extension\Extension $theme
   *   An enabled theme.
   *
   * @return bool
   *   TRUE if the theme is a custom theme.
   */
  private function isCustomTheme(Extension $theme) {
    $custom_path = $this->getCustomThemePath();

    if (FALSE !== strpos($theme->getPath(), $custom_path)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Path of the theme directory to look for themes.
   *
   * @return string
   *   The directory path.
   */
  private function getCustomThemePath() {
    return 'themes/custom';
  }

}
