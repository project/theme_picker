<?php

namespace Drupal\theme_picker\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThemePicker.
 *
 */
class ThemePicker implements ThemeNegotiatorInterface {

  /**
   * Config Factory Interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Admin Context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * ThemePicker constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory interface.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   AdminContext.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AdminContext $admin_context) {
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.admin_context')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $is_admin = $this->adminContext->isAdminRoute($route_match->getRouteObject());

    return !$is_admin && $this->negotiateRoute($route_match) ? TRUE : FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {

    return $this->negotiateRoute($route_match) ?: NULL;
  }

  /**
   * Function that does all of the work in selecting a theme.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route.
   *
   * @return bool|string
   *   Return FALSE to use the default theme, otherwise the name of the
   *   selected theme for the page.
   */
  private function negotiateRoute(RouteMatchInterface $route_match) {

    /** @var \Drupal\node\Entity\Node $node */
    $node = $route_match->getParameter('node');

    if (!empty($node) && $node->hasField('field_theme_picker_key')) {
      $theme = $node->get('field_theme_picker_key')->value;

      if (!empty($theme)) {
        return $theme;
      }
    }

    // Use default theme.
    return FALSE;
  }

}
